#include "stdafx.h"
#include "CoordinateSystem.hpp"

constexpr float Pi = 3.14159265358979323846f;
constexpr float MetersToPixelsFactor = 80.0f;
constexpr float PixelsToMetersFactor = 1.0f / MetersToPixelsFactor;

sf::Vector2f CoordinateSystem::transformToViewPosition(b2Vec2 position)
{
	return sf::Vector2f(position.x * MetersToPixelsFactor, -position.y * MetersToPixelsFactor);
}

sf::Vector2f CoordinateSystem::transformToViewDimensions(b2Vec2 dimensions)
{
	return sf::Vector2f(dimensions.x * MetersToPixelsFactor, dimensions.y * MetersToPixelsFactor);
}

float CoordinateSystem::transformToViewAngle(float32 angle)
{
	return -angle * (180.0f / Pi);
}

b2Vec2 CoordinateSystem::transformToModelPosition(sf::Vector2f position)
{
	return b2Vec2(position.x * PixelsToMetersFactor, -position.y * PixelsToMetersFactor);
}

b2Vec2 CoordinateSystem::transformToModelDimensions(sf::Vector2f dimensions)
{
	return b2Vec2(dimensions.x * PixelsToMetersFactor, dimensions.y * PixelsToMetersFactor);
}

float32 CoordinateSystem::transformToModelAngle(float angle)
{
	return -angle * (Pi / 180.0f);
}
