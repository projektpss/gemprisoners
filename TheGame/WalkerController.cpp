#include "stdafx.h"

#include "WalkerController.hpp"

#include "Controlable.hpp"

bool WalkerController::canControl(const Controlable & controlable) const
{
	return controlable.isWalker();
}

void WalkerController::setControlable(Controlable & controlable)
{
	walker = controlable.getWalker();
}
