#include "stdafx.h"

#include "TaskExecutor.hpp"

std::list<TaskExecutor::Order> TaskExecutor::orders;

TaskExecutor::Order::Order(Task task, Period waitPeriod) :
	task(task),
	waitPeriod(waitPeriod)
{

}

void TaskExecutor::executeAfter(sf::Time time, Task task)
{
	orders.push_back(Order(task, Period(time)));
}

void TaskExecutor::refresh()
{
	auto order = orders.begin();
	while (order != orders.end())
	{
		if (order->waitPeriod.isFinished())
		{
			order->task();
			order = orders.erase(order);
		}
		else
		{
			++order;
		}
	}
}
