#include "stdafx.h"

#include "WeaponOwner.hpp"

#include <stdexcept>

#include "Weapon.hpp"

WeaponOwner::WeaponOwner(Puppet & puppet):
	puppet(puppet)
{

}

WeaponOwner::~WeaponOwner()
{
	weapon->clearOwner();
}

Weapon & WeaponOwner::getWeapon()
{
	return *weapon;
}

void WeaponOwner::setWeapon(Weapon & weapon)
{
	if (weapon.hasOwner())
	{
		throw std::runtime_error("This weapon already has owner");
	}

	this->weapon->clearOwner();

	this->weapon = &weapon;
	weapon.setOwner(puppet);
}

void WeaponOwner::attack()
{
	weapon->attack();
}

bool WeaponOwner::canAttack() const
{
	return weapon->canAttack();
}
