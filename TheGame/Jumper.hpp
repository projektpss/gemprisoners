#pragma once

class Jumper
{
public:

	virtual void jump() = 0;
	virtual bool canJump() = 0;
};
