#pragma once

class Attack
{
public:

	enum class Type
	{
		Wood,
		Steel,
		Fire
	};

	Attack(int value, Type type);

	int getValue() const;
	Type getType() const;

private:

	int value;
	Type type;
};
