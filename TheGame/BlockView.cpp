#include "stdafx.h"

#include "BlockView.hpp"

#include "CoordinateSystem.hpp"

BlockView::BlockView(const Block & block):
	block(block),
	position(
		CoordinateSystem::transformToViewPosition(
			block.getPosition()
		)
	)
{

}
