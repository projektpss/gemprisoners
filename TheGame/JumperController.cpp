#include "stdafx.h"

#include "JumperController.hpp"

#include "Controlable.hpp"

bool JumperController::canControl(const Controlable & controlable) const
{
	return controlable.isJumper();
}

void JumperController::setControlable(Controlable & controlable)
{
	jumper = controlable.getJumper();
}
