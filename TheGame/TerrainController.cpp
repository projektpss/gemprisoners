#include "stdafx.h"

#include "TerrainController.hpp"

#include "TerrainLoader.hpp"

TerrainController::TerrainController(Terrain & model, TerrainView & view) :
	model(model),
	view(view)
{
	model.subscribe(view);
}

void TerrainController::loadFromFile(std::string filename)
{
	TerrainLoader::load(model, filename);
}
