#include "stdafx.h"

#include "Terrain.hpp"

#include <algorithm>

Terrain::Terrain(b2World & world):
	world(world)
{

}

Terrain::~Terrain()
{
	removeAllBlocks();
}

void Terrain::add(std::unique_ptr<Block> block)
{
	notifyOnCreateObservers(*block);
	blocks.push_back(std::move(block));
}

void Terrain::remove(Block & block)
{
	BlocksList::iterator toErase = std::find_if(
		blocks.begin(),
		blocks.end(),
		[&block](BlocksListElement & blockFromList)
		{
			return blockFromList.get() == &block;
		}
	);

	if (toErase == blocks.end())
	{
		return;
	}

	notifyOnDestroyObservers(*toErase->get());
	blocks.erase(toErase);
}

b2World & Terrain::getWorld()
{
	return world;
}

void Terrain::removeAllBlocks()
{
	for (auto & block : blocks)
	{
		notifyOnDestroyObservers(*block);
	}

	blocks.clear();
}

void Terrain::subscribe(TerrainObserver & observer)
{
	observers.push_back(&observer);
}

void Terrain::unsubscribe(TerrainObserver & observer)
{
	observers.erase(
		std::remove(observers.begin(), observers.end(), &observer),
		observers.end()
	);
}

void Terrain::notifyOnCreateObservers(const Block & createdBlock) const
{
	for (auto & observer : observers)
	{
		observer->onBlockCreate(createdBlock);
	}
}

void Terrain::notifyOnDestroyObservers(const Block & blockToDestroy) const
{
	for (auto & observer : observers)
	{
		observer->onBlockDestroy(blockToDestroy);
	}
}
