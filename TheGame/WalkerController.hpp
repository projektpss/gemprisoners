#pragma once

#include "Controller.hpp"

class Walker;

class WalkerController : public Controller
{
public:

	bool canControl(const Controlable & controlable) const override;
	void setControlable(Controlable & controlable) override;

protected:

	Walker * walker = nullptr;
};
