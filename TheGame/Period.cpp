#include "stdafx.h"

#include "Period.hpp"

#include "GameTimer.hpp"

Period::Period(sf::Time activeTime)
{
	activateOn(activeTime);
}

bool Period::isActive() const
{
	return GameTimer::getTime() < timeOut;
}

bool Period::isFinished() const
{
	return !isActive();
}

void Period::activateOn(sf::Time time)
{
	timeOut = GameTimer::getTime() + time;
}
