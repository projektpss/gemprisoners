#pragma once

#include <Box2D\Box2D.h>

#include "Block.hpp"
#include "GroundBlockContactable.hpp"

class GroundBlock: public Block
{
public:

	GroundBlock(b2World & world, b2Vec2 position);

private:

	b2PolygonShape shape;
	GroundBlockContactable contactable;
};
