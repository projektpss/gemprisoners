#pragma once

#include <type_traits>

#include "Box2D/Common/b2Math.h"

#include "GridPosition.hpp"
#include "Period.hpp"

class Puppet;
class Attacker;
class Walker;

class SkeletonAi
{
public:

	template <
		typename T,
		typename = std::enable_if<
			   std::is_base_of<Puppet,   T>::value
			&& std::is_base_of<Attacker, T>::value
			&& std::is_base_of<Walker,   T>::value,
			T
		>
	>
	SkeletonAi(T & slave, float patrolRange) :
		SkeletonAi(slave, slave, slave, patrolRange)
	{

	}

	void setTarget(Puppet & target);
	void run();

private:

	SkeletonAi(
		Puppet & puppet,
		Attacker & attacker,
		Walker & walker,
		float patrolRange
	);

	enum class BehaviourMode
	{
		Patrol,
		Offensive,
		Follow,
	};

	enum class MoveDirection
	{
		Left,
		Right,
		Stop,
	};

	// Slave interfaces
	Puppet * slaveAsPuppet;
	Attacker * slaveAsAttacker;
	Walker * slaveAsWalker;

	Puppet * target;
	b2Vec2 slavePosition;
	Period patrolDelay;

	float leftPatrolLimit;
	float rightPatrolLimit;
	float leftOffensiveLimit;
	float rightOffensiveLimit;

	float patrolRange = 1.0;
	float minTargetDistance = 0.0;
	float offensiveArea = 3.0;

	BehaviourMode behaviorMode = BehaviourMode::Patrol;
	MoveDirection moveDirection = MoveDirection::Left;

	void updateBehaviorMode();

	void patrol();
	void offensive();

	void move();
	void followTarget();
	void updateSlavePosition();

	float getTargetDistance() const;
};
