#include "stdafx.h"

#include "GameTimer.hpp"

sf::Time GameTimer::now;

sf::Time GameTimer::getTime()
{
	return now;
}

void GameTimer::registerTimeLapse(sf::Time elapsedTime)
{
	now += elapsedTime;
}
