#pragma once

#include "SFML\Graphics\Drawable.hpp"

#include "Block.hpp"

class BlockView: public sf::Drawable
{
public:

	BlockView(const Block & block);

protected:

	const Block & block;
	sf::Vector2f position;

	friend class TerrainView;
};
