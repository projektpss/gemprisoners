#include "stdafx.h"

#include "Puppet.hpp"

#include "Box2D/Box2D.h"

Puppet::Puppet(b2World & world, b2Vec2 position)
{
	b2BodyDef bodyDef;
	bodyDef.position = position;
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;
	body = world.CreateBody(&bodyDef);
}

Puppet::~Puppet()
{
	body->GetWorld()->DestroyBody(body);
}

b2Body * Puppet::getBody()
{
	return body;
}

b2Vec2 Puppet::getPosition() const
{
	return body->GetPosition();
}

b2Vec2 Puppet::getLinearVelocity() const
{
	return body->GetLinearVelocity();
}

DirectionOfLooking Puppet::getDirectionOfLooking() const
{
	return directionOfLooking;
}

void Puppet::setDirectionOfLooking(DirectionOfLooking directionOfLooking)
{
	this->directionOfLooking = directionOfLooking;
}

void Puppet::applyForceToCenter(b2Vec2 force)
{
	body->ApplyForceToCenter(force, true);
}

void Puppet::applyLinearImpulseToCenter(b2Vec2 impulse)
{
	body->ApplyLinearImpulseToCenter(impulse, true);
}

void Puppet::setLinearVelocity(b2Vec2 velocity)
{
	body->SetLinearVelocity(velocity);
}

void Puppet::createFixture(b2FixtureDef & fixtureDef)
{
	body->CreateFixture(&fixtureDef);
}
