#include "stdafx.h"

#include "AttackerController.hpp"

#include "Controlable.hpp"

bool AttackerController::canControl(const Controlable & controlable) const
{
	return controlable.isAttacker();
}

void AttackerController::setControlable(Controlable & controlable)
{
	attacker = controlable.getAttacker();
}
