#pragma once

#include "SFML/Graphics/Color.hpp"
#include "SFML/Graphics/Drawable.hpp"

class b2Body;

class BodyView : public sf::Drawable
{
public:

	BodyView(const b2Body & body, sf::Color color);

	void draw(sf::RenderTarget & target, sf::RenderStates states) const override;

private:

	const b2Body * body;
	sf::Color color;
};
