#include "stdafx.h"

#include "ContactListener.hpp"

#include "Box2D/Box2D.h"

#include "Contactable.hpp"

namespace
{

class ContactListener : public b2ContactListener
{
public:

	void BeginContact(b2Contact * contact) override;
	void EndContact(b2Contact * contact) override;
};

ContactListener contactListenerInstance;

}

b2ContactListener * const contactListener = &contactListenerInstance;

namespace
{

void ContactListener::BeginContact(b2Contact * contact)
{
	Contactable & first = *static_cast<Contactable *>(contact->GetFixtureA()->GetUserData());
	Contactable & second = *static_cast<Contactable *>(contact->GetFixtureB()->GetUserData());

	first.handleBeginContact(second);
	second.handleBeginContact(first);
}

void ContactListener::EndContact(b2Contact * contact)
{
	Contactable & first = *static_cast<Contactable *>(contact->GetFixtureA()->GetUserData());
	Contactable & second = *static_cast<Contactable *>(contact->GetFixtureB()->GetUserData());

	first.handleEndContact(second);
	second.handleEndContact(first);
}

}
