#pragma once

class Controlable;

class Controller
{
public:

	virtual void run() = 0;

	virtual bool canControl(const Controlable & controlable) const = 0;
	virtual void setControlable(Controlable & controlable) = 0;

	virtual ~Controller() = default;
};
