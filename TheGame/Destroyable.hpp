#pragma once

class Destroyable
{
public:

	// Destroys given destroyable object
	virtual void operator()() = 0;

	virtual ~Destroyable() = default;
};
