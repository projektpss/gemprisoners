#pragma once

class Attacker;
class Jumper;
class Walker;

class Controlable
{
public:

	bool isWalker() const;
	bool isJumper() const;
	bool isAttacker() const;

	virtual Walker * getWalker();
	virtual Jumper * getJumper();
	virtual Attacker * getAttacker();

	virtual ~Controlable() = default;

protected:

	static constexpr int WalkerType   = 0x01;
	static constexpr int JumperType   = 0x02;
	static constexpr int AttackerType = 0x04;

	virtual int getType() const = 0;
};
