#pragma once

#include "Attackable.hpp"
#include "Jumpable.hpp"

class Contactable :
	public Jumpable,
	public Attackable
{
public:

	virtual void handleBeginContact(Contactable & contactable);
	virtual void handleEndContact(Contactable & contactable);

	virtual ~Contactable() = default;
};

extern Contactable * const NullContactable;
