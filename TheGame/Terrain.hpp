#pragma once

#include <list>
#include <memory>
#include <vector>

#include "Box2D/Dynamics/b2World.h"

#include "Block.hpp"
#include "TerrainObserver.hpp"

class Terrain
{
public:

	Terrain(b2World & world);
	~Terrain();

	void add(std::unique_ptr<Block> block);
	void remove(Block & block);
	b2World & getWorld();

	void removeAllBlocks();

	void subscribe(TerrainObserver & observer);
	void unsubscribe(TerrainObserver & observer);

private:

	using BlocksListElement = std::unique_ptr<Block>;
	using BlocksList = std::list<BlocksListElement>;

	b2World & world;
	BlocksList blocks;
	std::vector<TerrainObserver *> observers;

	void notifyOnCreateObservers(const Block & createdBlock) const;
	void notifyOnDestroyObservers(const Block & blockToDestroy) const;
};
