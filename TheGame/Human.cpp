#include "stdafx.h"

#include "Human.hpp"

#include "Box2D/Box2D.h"

#include "Attack.hpp"

Human::Human(b2World & world, b2Vec2 position) :
	puppet(world, position, *this),
	health(30)
{

}

HumanPuppet & Human::getPuppet()
{
	return puppet;
}

const Health & Human::getHealth() const
{
	return health;
}

bool Human::isAttackable() const
{
	return true;
}

void Human::takeAttack(Attack & attack)
{
	health -= attack.getValue();
}
