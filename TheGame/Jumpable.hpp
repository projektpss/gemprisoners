#pragma once

class Jumpable
{
public:

	virtual bool isJumpable() const;
	virtual ~Jumpable() = default;
};
