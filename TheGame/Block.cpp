#include "stdafx.h"

#include "Block.hpp"

Block::Block(b2World & world, b2Vec2 position, Type type):
	type(type)
{
	b2BodyDef bodyDef;
	bodyDef.position = position;

	body = world.CreateBody(&bodyDef);
}

Block::~Block()
{
	b2World & world = getWorld();
	world.DestroyBody(body);
}

const b2Body * Block::getBody() const
{
	return body;
}

b2Vec2 Block::getPosition() const
{
	return body->GetPosition();
}

Block::Type Block::getType() const
{
	return type;
}

b2World & Block::getWorld()
{
	return *body->GetWorld();
}
