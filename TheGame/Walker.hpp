#pragma once

class Walker
{
public:

	virtual void goLeft() = 0;
	virtual void goRight() = 0;
};
