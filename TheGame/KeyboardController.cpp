#include "stdafx.h"

#include "KeyboardController.hpp"

#include "SFML/Window/Keyboard.hpp"

#include "Attacker.hpp"
#include "Walker.hpp"
#include "Jumper.hpp"

void KeyboardController::KeyboardAttackerController::run()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		attacker->attack();
	}
}

void KeyboardController::KeyboardWalkerController::run()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		walker->goLeft();
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		walker->goRight();
	}
}

void KeyboardController::KeyboardJumperController::run()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		jumper->jump();
	}
}

void KeyboardController::run()
{
	for (auto & controller : activeControllers)
	{
		controller->run();
	}
}

bool KeyboardController::canControl(const Controlable & controlable) const
{
	return attackerController.canControl(controlable)
		| walkerController.canControl(controlable)
		| jumperController.canControl(controlable);
}

void KeyboardController::setControlable(Controlable & controlable)
{
	activeControllers.clear();

	if (attackerController.canControl(controlable))
	{
		attackerController.setControlable(controlable);
		activeControllers.push_back(&attackerController);
	}

	if (walkerController.canControl(controlable))
	{
		walkerController.setControlable(controlable);
		activeControllers.push_back(&walkerController);
	}

	if (jumperController.canControl(controlable))
	{
		jumperController.setControlable(controlable);
		activeControllers.push_back(&jumperController);
	}

	if (activeControllers.empty())
	{
		throw std::runtime_error("Can't control this controlable");
	}
}
