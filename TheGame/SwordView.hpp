#pragma once

#include <memory>

#include "SFML/Graphics/Drawable.hpp"

#include "SwordObserver.hpp"

class BodyView;
class Sword;

class SwordView :
	public sf::Drawable,
	public SwordObserver
{
public:

	SwordView(Sword & sword);

	void draw(sf::RenderTarget & target, sf::RenderStates states) const override;

	void onHitBoxCreate(SwordHitBox & hitBox) override;
	void onHitBoxDestroy(SwordHitBox & hitBox) override;

private:

	std::unique_ptr<BodyView> hitBoxView;
};
