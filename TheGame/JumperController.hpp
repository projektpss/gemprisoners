#pragma once

#include "Controller.hpp"

class Jumper;

class JumperController : public Controller
{
public:

	bool canControl(const Controlable & controlable) const override;
	void setControlable(Controlable & controlable) override;

protected:

	Jumper * jumper = nullptr;
};
