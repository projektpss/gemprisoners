#pragma once

#include <functional>
#include <list>

#include "SFML/System/Time.hpp"

#include "Period.hpp"

class TaskExecutor
{
public:

	using Task = std::function<void()>;

	static void executeAfter(sf::Time time, Task task);
	static void refresh();

private:

	struct Order
	{
		Order(Task task, Period waitPeriod);

		Task task;
		Period waitPeriod;
	};

	static std::list<Order> orders;
};
