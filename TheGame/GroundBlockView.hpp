#pragma once

#include "BlockView.hpp"
#include "GroundBlock.hpp"

class GroundBlockView: public BlockView
{
public:

	GroundBlockView(const GroundBlock & block);

protected:

	void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
};
