#pragma once

#include "Contactable.hpp"

class FootSensorContactable: public Contactable
{
public:

	void handleBeginContact(Contactable & contactable) override;
	void handleEndContact(Contactable & contactable) override;
	bool canJump();

private:

	int jumpablesCnt = 0;
};
