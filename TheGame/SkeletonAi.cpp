#include "stdafx.h"

#include "SkeletonAi.hpp"

#include <cmath>

#include "Puppet.hpp"
#include "Attacker.hpp"
#include "Walker.hpp"

SkeletonAi::SkeletonAi(Puppet & puppet, Attacker & attacker, Walker & walker, float patrolRange):
	slaveAsPuppet(&puppet),
	slaveAsAttacker(&attacker),
	slaveAsWalker(&walker),
	patrolRange(patrolRange)

{
	updateSlavePosition();

	rightPatrolLimit = slavePosition.x;
	leftPatrolLimit = rightPatrolLimit - patrolRange;

	rightOffensiveLimit = rightPatrolLimit + offensiveArea;
	leftOffensiveLimit = leftPatrolLimit - offensiveArea;
}

void SkeletonAi::setTarget(Puppet & target)
{
	this->target = &target;
}

void SkeletonAi::run()
{
	updateBehaviorMode();
	updateSlavePosition();
	switch (behaviorMode)
	{
	case BehaviourMode::Patrol:
		this->patrol();
		break;
	case BehaviourMode::Offensive:
		this->offensive();
		break;
	default:
		break;
	}
}

void SkeletonAi::updateBehaviorMode()
{
	if ((target->getPosition().x > leftOffensiveLimit) && (target->getPosition().x < rightOffensiveLimit))
	{
		behaviorMode = BehaviourMode::Offensive;
	}
	else
	{
		behaviorMode = BehaviourMode::Patrol;
	}
}

void SkeletonAi::offensive()
{
	followTarget();

	if (slaveAsAttacker->canAttack())
	{
		if (slavePosition.x > target->getPosition().x)
		{
			slaveAsPuppet->setDirectionOfLooking(DirectionOfLooking::Left);
		}
		else
		{
			slaveAsPuppet->setDirectionOfLooking(DirectionOfLooking::Right);
		}

		slaveAsAttacker->attack();
	}
}

void SkeletonAi::patrol()
{
	if (slavePosition.x > rightPatrolLimit && moveDirection != MoveDirection::Left)
	{
		if (moveDirection == MoveDirection::Right)
		{
			patrolDelay.activateOn(sf::seconds(1));
			moveDirection = MoveDirection::Stop;
		}
		if (patrolDelay.isFinished())
		{
			moveDirection = MoveDirection::Left;
		}
	}
	else if (slavePosition.x < leftPatrolLimit && moveDirection != MoveDirection::Right)
	{
		if (moveDirection == MoveDirection::Left)
		{
			patrolDelay.activateOn(sf::seconds(1));
			moveDirection = MoveDirection::Stop;
		}
		if (patrolDelay.isFinished())
		{
			moveDirection = MoveDirection::Right;
		}
	}
	move();
}

void SkeletonAi::move()
{
	switch (moveDirection)
	{
	case MoveDirection::Left:
		slaveAsWalker->goLeft();
		break;
	case MoveDirection::Right:
		slaveAsWalker->goRight();
		break;
	default:
		break;
	}
}

void SkeletonAi::followTarget()
{
	if (getTargetDistance() > minTargetDistance)
	{
		if (target->getPosition().x < slavePosition.x)
		{
			moveDirection = MoveDirection::Left;
		}
		else
		{
			moveDirection = MoveDirection::Right;
		}
	}
	else
	{
		if (target->getPosition().x < slavePosition.x)
		{
			moveDirection = MoveDirection::Right;
		}
		else
		{
			moveDirection = MoveDirection::Left;
		}
	}
	move();
}

void SkeletonAi::updateSlavePosition()
{
	slavePosition = slaveAsPuppet->getPosition();
}

float SkeletonAi::getTargetDistance() const
{
	return fabs(target->getPosition().x - slavePosition.x);
}
