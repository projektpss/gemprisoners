#pragma once

class SwordHitBox;

class SwordObserver
{
public:

	virtual void onHitBoxCreate(SwordHitBox & hitBox) = 0;
	virtual void onHitBoxDestroy(SwordHitBox & hitBox) = 0;
};
