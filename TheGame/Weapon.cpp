#include "stdafx.h"

#include "Weapon.hpp"

extern b2World world;

Weapon NullWeapon(world);

Weapon::Weapon(b2World & world):
	world(world)
{

}

bool Weapon::hasOwner() const
{
	return owner != nullptr;
}

void Weapon::setOwner(Puppet & owner)
{
	this->owner = &owner;
}

void Weapon::attack()
{
	// Do nothing
}

bool Weapon::canAttack() const
{
	return false;
}

void Weapon::clearOwner()
{
	owner = nullptr;
}
