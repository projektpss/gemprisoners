#include "stdafx.h"

#include <exception>

#include "SFML\Graphics.hpp"

#include "TerrainView.hpp"

#include "GroundBlock.hpp"
#include "GroundBlockView.hpp"

namespace
{
constexpr char NonExistentViewOfBlockType[] = "Non-existent view for block of this type";
}

void TerrainView::add(std::unique_ptr<const BlockView> block)
{
	blocks.push_back(std::move(block));
}

void TerrainView::remove(const Block & block)
{
	blocks.remove_if(
		[&block](std::unique_ptr<const BlockView> & blockInContainer)
		{
			return &blockInContainer->block == &block;
		}
	);
}

void TerrainView::onBlockCreate(const Block & block)
{
	switch (block.getType())
	{
	case Block::Type::Ground:
		add(
			std::make_unique<const GroundBlockView>(
				static_cast<const GroundBlock &>(block)
			)
		);
		break;

	default:
		throw std::domain_error(NonExistentViewOfBlockType);
	}
}

void TerrainView::onBlockDestroy(const Block & block)
{
	remove(block);
}

void TerrainView::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (const auto & block : blocks)
	{
		target.draw(*block, states);
	}
}
