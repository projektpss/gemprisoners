#pragma once

#include "Controller.hpp"

class Attacker;

class AttackerController : public Controller
{
public:

	bool canControl(const Controlable & controlable) const override;
	void setControlable(Controlable & controlable) override;

protected:

	Attacker * attacker = nullptr;
};
