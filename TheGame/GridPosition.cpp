#include "stdafx.h"

#include "GridPosition.hpp"

#include <cmath>

#include "CoordinateSystem.hpp"

GridPosition::GridPosition(int x, int y):
	x(x),
	y(y)
{

}

GridPosition::GridPosition(sf::Vector2f position)
{
	sf::Vector2f blockDimensions = CoordinateSystem::transformToViewDimensions(
		b2Vec2(
			BlockDimensions::Width,
			BlockDimensions::Height
		)
	);

	x = static_cast<int>(+std::round(position.x / blockDimensions.x));
	y = static_cast<int>(-std::round(position.y / blockDimensions.y));
}

GridPosition::operator sf::Vector2f() const
{
	return CoordinateSystem::transformToViewPosition(
		static_cast<b2Vec2>(*this)
	);
}

GridPosition::operator b2Vec2() const
{
	return b2Vec2(
		x * BlockDimensions::Width,
		y * BlockDimensions::Height
	);
}
