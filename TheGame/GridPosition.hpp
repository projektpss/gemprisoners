#pragma once

#include "SFML\System\Vector2.hpp"
#include "Box2D\Common\b2Math.h"

#include "BlockDimensions.hpp"

/*
	Helper class to define positions referencing to grid. GridPosition is
	convertable to sf::Vector2f and b2Vec2.

	Axis directions are defined like in Cartesian system:

	            ^ Y
	            |
	 -2   -1    0    1    2
	--+----+----+----+----+---  2
	  |    |    |    |    |
	--+----+----+----+----+---  1
	  |    |    |    |    |
	--+----+----+----+----+---  0 --> X
	  |    |    |    |    |
	--+----+----+----+----+--- -1
	  |    |    |    |    |
	--+----+----+----+----+--- -2
	  |    |    |    |    |
*/

class GridPosition
{
public:

	int x, y;

	GridPosition(int x, int y);
	GridPosition(sf::Vector2f position);
	operator sf::Vector2f() const;
	operator b2Vec2() const;
};
