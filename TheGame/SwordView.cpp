#include "stdafx.h"

#include "SwordView.hpp"

#include "SFML/Graphics.hpp"

#include "BodyView.hpp"
#include "Sword.hpp"
#include "SwordHitBox.hpp"

SwordView::SwordView(Sword & sword)
{
	sword.subscribe(*this);
}

void SwordView::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	if (hitBoxView == nullptr)
	{
		return;
	}

	target.draw(*hitBoxView);
}

void SwordView::onHitBoxCreate(SwordHitBox & hitBox)
{
	hitBoxView = std::make_unique<BodyView>(hitBox.getBody(), sf::Color::White);
}

void SwordView::onHitBoxDestroy(SwordHitBox & hitbox)
{
	hitBoxView.reset();
}
