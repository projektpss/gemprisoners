#include "stdafx.h"

#include "SkeletonPuppet.hpp"

#include "Box2D/Box2D.h"

SkeletonPuppet::SkeletonPuppet(b2World & world, b2Vec2 position, Contactable & mainContactable) :
	Puppet(world, position),
	PuppetWalker(static_cast<Puppet &>(*this)),
	PuppetJumper(static_cast<Puppet &>(*this)),
	WeaponOwner(static_cast<Puppet &>(*this))
{
	// Create main fixture
	b2Vec2 vertices[6];
	vertices[0].Set(-0.20f, 0.40f);
	vertices[1].Set(0.20f, 0.40f);
	vertices[2].Set(0.20f, -0.35f);
	vertices[3].Set(0.12f, -0.40f);
	vertices[4].Set(-0.12f, -0.40f);
	vertices[5].Set(-0.20f, -0.35f);

	b2PolygonShape shape;
	shape.Set(vertices, 6);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.5f;
	fixtureDef.userData = &mainContactable;
	createFixture(fixtureDef);

	// Create foot sensor
	shape.SetAsBox(0.19f, 0.02f, b2Vec2(0.0f, -0.42f), 0.0f);

	b2FixtureDef footFixtureDef;
	footFixtureDef.shape = &shape;
	footFixtureDef.isSensor = true;
	footFixtureDef.userData = &footSensorContactable;

	createFixture(footFixtureDef);

	// Set walking and jumping parameters
	setForceOfJump(1.5f);
	setForceOfWalk(4.0f);
	setVelocityOfWalk(2.0f);
}

int SkeletonPuppet::getType() const
{
	return Controlable::WalkerType
		| Controlable::JumperType
		| Controlable::AttackerType;
}

Walker * SkeletonPuppet::getWalker()
{
	return this;
}

Jumper * SkeletonPuppet::getJumper()
{
	return this;
}

Attacker * SkeletonPuppet::getAttacker()
{
	return this;
}
