#pragma once

#include <vector>

#include "Controller.hpp"
#include "AttackerController.hpp"
#include "WalkerController.hpp"
#include "JumperController.hpp"

class KeyboardController : public Controller
{
public:

	void run() override;
	bool canControl(const Controlable & controlable) const override;
	void setControlable(Controlable & controlable) override;

private:

	class KeyboardAttackerController : public AttackerController { void run() override; };
	class KeyboardWalkerController   : public WalkerController   { void run() override; };
	class KeyboardJumperController   : public JumperController   { void run() override; };

	KeyboardAttackerController attackerController;
	KeyboardWalkerController walkerController;
	KeyboardJumperController jumperController;

	std::vector<Controller *> activeControllers;
};
