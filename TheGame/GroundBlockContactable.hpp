#pragma once

#include "Contactable.hpp"

class GroundBlockContactable: public Contactable
{
public:

	bool isJumpable() const override;
};
