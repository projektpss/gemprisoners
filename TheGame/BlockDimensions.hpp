#pragma once

class BlockDimensions
{
public:

	static constexpr float Width = 0.5f;
	static constexpr float Height = Width; // Blocks are squares
};
