#include "stdafx.h"

#include "Drawer.hpp"

#include <Box2D\Box2D.h>
#include <SFML\Graphics.hpp>

#include "CoordinateSystem.hpp"

void Drawer::drawBody(sf::RenderTarget & target, const b2Body * body, sf::Color color)
{
	for (const b2Fixture * fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		if (fixture->GetShape()->m_type == b2Shape::e_polygon)
		{
			const b2PolygonShape * bodyShape = reinterpret_cast<const b2PolygonShape *>(fixture->GetShape());
			b2Transform transform = body->GetTransform();

			sf::ConvexShape convex(bodyShape->m_count);
			for (int i = 0; i < bodyShape->m_count; ++i)
			{
				convex.setPoint(i, CoordinateSystem::transformToViewPosition(b2Mul(transform, bodyShape->m_vertices[i])));
			}
			convex.setOutlineColor(color);
			convex.setFillColor(sf::Color(color.r, color.g, color.b, 64));
			convex.setOutlineThickness(-3.0f);

			target.draw(convex);
		}
	}
}
