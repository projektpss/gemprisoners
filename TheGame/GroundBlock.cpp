#include "stdafx.h"

#include "GroundBlock.hpp"

GroundBlock::GroundBlock(b2World & world, b2Vec2 position):
	Block(world, position, Block::Type::Ground)
{
	shape.SetAsBox(Width / 2, Height / 2);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.userData = &contactable;
	body->CreateFixture(&fixtureDef);
}
