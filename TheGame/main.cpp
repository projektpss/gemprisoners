﻿#include "stdafx.h"

#include "SFML\Graphics.hpp"

#include "Box2D\Box2D.h"
#include <cstdio>
#include <memory>
#include <vector>

#include "GameTimer.hpp"
#include "Period.hpp"
#include "TaskExecutor.hpp"
#include "CoordinateSystem.hpp"
#include "Drawer.hpp"
#include "GridPosition.hpp"
#include "Terrain.hpp"
#include "TerrainView.hpp"
#include "TerrainController.hpp"
#include "ContactListener.hpp"
#include "Destroyer.hpp"
#include "Human.hpp"
#include "Skeleton.hpp"

#include "Controller.hpp"
#include "Sword.hpp"
#include "SwordView.hpp"
#include "KeyboardController.hpp"
#include "HealthView.hpp"

#include "SkeletonAi.hpp"

b2Vec2 gravity(0.0f, -10.0f);
b2World world(gravity);

std::list<std::unique_ptr<sf::Drawable>> drawables;
std::list<std::unique_ptr<sf::Drawable>> headsUpDisplay;

int main()
{
	world.SetContactListener(contactListener);

	Human human(world, b2Vec2(0.0f, 4.0f));
	Skeleton skeleton(world, GridPosition(15, -7));

	HumanPuppet & humanPuppet = human.getPuppet();

	Terrain terrain(world);
	TerrainView terrainView;
	TerrainController terrainController(terrain, terrainView);
	terrainController.loadFromFile("Terrains/example_terrain.txt");

	BlockDestroyable::setTerrain(terrain);

	// Prepare for simulation. Typically we use a time step of 1/60 of a
	// second (60Hz) and 10 iterations. This provides a high quality simulation
	// in most game scenarios.
	float32 timeStep = 1.0f / 60.0f;
	int32 velocityIterations = 6;
	int32 positionIterations = 2;

	sf::RenderWindow window(sf::VideoMode(960, 540), "SFML works!");

	sf::View view(
		CoordinateSystem::transformToViewPosition(b2Vec2(0.0f, 0.0f)),
		sf::Vector2f(
			static_cast<float>(window.getSize().x),
			static_cast<float>(window.getSize().y)
		)
	);
	window.setView(view);

	sf::Clock gameClock;
	const sf::Time GameStep = sf::seconds(timeStep);
	sf::Time nextGameStep = gameClock.getElapsedTime();

	headsUpDisplay.push_back(
		std::make_unique<HealthView>(
			human.getHealth(),
			sf::Vector2f(10.0f, 10.0f)
		)
	);

	Sword sword(world);
	humanPuppet.setWeapon(sword);

	drawables.push_back(
		std::make_unique<SwordView>(sword)
	);

	Sword swordOfSkeleton(world);
	skeleton.getPuppet().setWeapon(swordOfSkeleton);

	drawables.push_back(
		std::make_unique<SwordView>(swordOfSkeleton)
	);

	std::list<Controller *> controllers;

	KeyboardController keyboardController;
	if (keyboardController.canControl(humanPuppet))
	{
		keyboardController.setControlable(humanPuppet);
		controllers.push_back(&keyboardController);
	}

	SkeletonAi skeletonAi(
		skeleton.getPuppet(),
		1.0
	);

	skeletonAi.setTarget(human.getPuppet());

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		while (gameClock.getElapsedTime() >= nextGameStep)
		{
			for (auto & controller : controllers)
			{
				controller->run();
			}

			skeletonAi.run();

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{
				window.close();
			}

			nextGameStep += GameStep;

			b2Vec2 cameraPosition = humanPuppet.getPosition();
			cameraPosition.y += 0.5f;
			view.setCenter(CoordinateSystem::transformToViewPosition(cameraPosition));
			window.setView(view);

			world.Step(timeStep, velocityIterations, positionIterations);
			GameTimer::registerTimeLapse(sf::seconds(timeStep));
			TaskExecutor::refresh();
			Destroyer::destroyRegistered();
		}

		window.clear();

		// Draw game view
		window.setView(view);

		window.draw(terrainView);
		Drawer::drawBody(window, humanPuppet.getBody(), sf::Color::Green);
		Drawer::drawBody(window, skeleton.getPuppet().getBody(), sf::Color::Red);

		for (auto & drawable : drawables)
		{
			window.draw(*drawable);
		}

		// Draw heads-up display [HUD]
		window.setView(window.getDefaultView());

		for (auto & drawable : headsUpDisplay)
		{
			window.draw(*drawable);
		}

		window.display();
	}

	terrain.removeAllBlocks();
	return 0;
}
