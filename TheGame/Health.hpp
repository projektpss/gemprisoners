#pragma once

#include <vector>

class HealthChangeObserver;
class DeadObserver;

class Health
{
public:

	Health(int maximum);
	Health(int value, int maximum);

	int getValue() const;
	int getMaximum() const;

	void setValue(int value);
	void setMaximum(int maximum);

	double getAsPercentOfMaximum() const;

	Health & operator+=(int healing);
	Health & operator-=(int damage);

	void subscribe(HealthChangeObserver & observer);
	void unsubscribe(HealthChangeObserver & observer);

	void subscribe(DeadObserver & observer);
	void unsubscribe(DeadObserver & observer);

private:

	int maximum;
	int value;

	std::vector<HealthChangeObserver *> healthChangeObservers;
	std::vector<DeadObserver *> deadObservers;

	int limitGivenValueToRange(int value) const;

	void notifyOnChangeObservers() const;
	void notifyOnDeadObservers() const;
};
