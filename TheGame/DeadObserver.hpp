#pragma once

class DeadObserver
{
public:

	virtual void onDead() = 0;
	virtual ~DeadObserver() = default;
};
