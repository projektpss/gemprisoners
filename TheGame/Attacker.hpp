#pragma once

class Attacker
{
public:

	virtual void attack() = 0;
	virtual bool canAttack() const = 0;
};
