#pragma once

#include "Block.hpp"

class TerrainObserver
{
public:

	virtual void onBlockCreate(const Block & block) = 0;
	virtual void onBlockDestroy(const Block & block) = 0;
};
