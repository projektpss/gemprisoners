#pragma once

#include "Destroyable.hpp"

class Block;
class Terrain;

class BlockDestroyable : public Destroyable
{
public:

	BlockDestroyable(Block & block);
	void operator()() override;
	static void setTerrain(Terrain & terrain);

private:

	static Terrain * terrain;
	Block & block;
};
