#pragma once

#include "Walker.hpp"

class Puppet;

class PuppetWalker : public Walker
{
public:

	PuppetWalker(Puppet & puppet);

	void setForceOfWalk(float forceOfWalk);
	void setVelocityOfWalk(float velocityOfWalk);

	// Inherited via Walker
	void goLeft() override;
	void goRight() override;

private:

	Puppet & puppet;

	float forceOfWalk = 0.0f;
	float velocityOfWalk = 0.0f;
};
