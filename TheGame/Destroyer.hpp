#pragma once

#include <list>
#include <memory>

#include "BlockDestroyable.hpp"

class Destroyable;
class Block;

class Destroyer
{
public:

	Destroyer() = delete;

	static void registerToDestroy(Block & block);

	static void destroyRegistered();

private:

	static std::list<std::unique_ptr<Destroyable>> destroyables;
};
