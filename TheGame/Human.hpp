#pragma once

#include "Health.hpp"
#include "HumanPuppet.hpp"

class Human :
	public Contactable
{
public:

	Human(b2World & world, b2Vec2 position);

	HumanPuppet & getPuppet();
	const Health & getHealth() const;

private:

	HumanPuppet puppet;
	Health health;

	bool isAttackable() const override;
	void takeAttack(Attack & attack) override;
};
