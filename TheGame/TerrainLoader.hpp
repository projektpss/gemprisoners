#pragma once

#include <string>

#include "Terrain.hpp"

class TerrainLoader
{
public:

	static void load(Terrain & terrain, std::string filename);
};
