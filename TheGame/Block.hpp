#pragma once

#include "Box2D\Common\b2Math.h"
#include "Box2D\Dynamics\b2World.h"
#include "Box2D\Dynamics\b2Body.h"

#include "BlockDimensions.hpp"

class Block
{
public:

	static constexpr float Width = BlockDimensions::Width;
	static constexpr float Height = BlockDimensions::Height;

	enum class Type
	{
		Ground,
	};

	Block(b2World & world, b2Vec2 position, Type type);
	~Block();

	const b2Body * getBody() const;
	b2Vec2 getPosition() const;
	Type getType() const;

protected:

	b2Body * body;

	b2World & getWorld();

private:

	Type type;
};
