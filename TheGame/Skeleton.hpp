#pragma once

#include "Health.hpp"
#include "SkeletonPuppet.hpp"

class Skeleton :
	public Contactable
{
public:

	Skeleton(b2World & world, b2Vec2 position);

	SkeletonPuppet & getPuppet();

private:

	SkeletonPuppet puppet;
	Health health;

	bool isAttackable() const override;
	void takeAttack(Attack & attack) override;
};
