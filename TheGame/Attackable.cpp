#include "stdafx.h"

#include <stdexcept>

#include "Attackable.hpp"

bool Attackable::isAttackable() const
{
	return false;
}

void Attackable::takeAttack(Attack & attack)
{
	throw std::domain_error("Call takeAttack on non-attackable object");
}
