#include "stdafx.h"

#include "PuppetWalker.hpp"

#include "Puppet.hpp"

PuppetWalker::PuppetWalker(Puppet & puppet):
	puppet(puppet)
{

}

void PuppetWalker::setForceOfWalk(float forceOfWalk)
{
	this->forceOfWalk = forceOfWalk;
}

void PuppetWalker::setVelocityOfWalk(float velocityOfWalk)
{
	this->velocityOfWalk = velocityOfWalk;
}

void PuppetWalker::goLeft()
{
	if (puppet.getLinearVelocity().x > -velocityOfWalk)
	{
		puppet.applyForceToCenter(
			b2Vec2(-forceOfWalk, 0.0f)
		);
	}

	puppet.setDirectionOfLooking(DirectionOfLooking::Left);
}

void PuppetWalker::goRight()
{
	if (puppet.getLinearVelocity().x < velocityOfWalk)
	{
		puppet.applyForceToCenter(
			b2Vec2(forceOfWalk, 0.0f)
		);
	}

	puppet.setDirectionOfLooking(DirectionOfLooking::Right);
}
