#include "stdafx.h"

#include "BlockDestroyable.hpp"

#include "Terrain.hpp"

Terrain * BlockDestroyable::terrain = nullptr;

BlockDestroyable::BlockDestroyable(Block & block):
	block(block)
{

}

void BlockDestroyable::operator()()
{
	terrain->remove(block);
}

void BlockDestroyable::setTerrain(Terrain & terrain)
{
	BlockDestroyable::terrain = &terrain;
}
