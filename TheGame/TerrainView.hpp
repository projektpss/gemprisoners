#pragma once

#include <list>
#include <memory>

#include "SFML\Graphics\Drawable.hpp"

#include "BlockView.hpp"
#include "TerrainObserver.hpp"

class TerrainView:
	public sf::Drawable,
	public TerrainObserver
{
public:

	void add(std::unique_ptr<const BlockView> block);
	void remove(const Block & block);

	void onBlockCreate(const Block & block) final override;
	void onBlockDestroy(const Block & block) final override;

private:

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	std::list<std::unique_ptr<const BlockView>> blocks;
};
