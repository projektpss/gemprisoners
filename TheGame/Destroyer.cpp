#include "stdafx.h"

#include "Destroyer.hpp"
#include "Destroyable.hpp"

#include <memory>

std::list<std::unique_ptr<Destroyable>> Destroyer::destroyables;

void Destroyer::registerToDestroy(Block & block)
{
	destroyables.push_back(
		std::make_unique<BlockDestroyable>(block)
	);
}

void Destroyer::destroyRegistered()
{
	if (destroyables.empty())
	{
		return;
	}

	for (auto & toDestroy : destroyables)
	{
		(*toDestroy)();
	}

	destroyables.clear();
}
