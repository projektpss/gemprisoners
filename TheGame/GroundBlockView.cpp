#include "stdafx.h"

#include "GroundBlockView.hpp"

#include <exception>
#include <SFML/Graphics.hpp>

#include "CoordinateSystem.hpp"

#include "Drawer.hpp"

GroundBlockView::GroundBlockView(const GroundBlock & block):
	BlockView(block)
{

}

void GroundBlockView::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	Drawer::drawBody(target, block.getBody(), sf::Color(16, 128, 0));
}
