#pragma once

class b2World;
class Puppet;

class Weapon
{
public:

	Weapon(b2World & world);

	bool hasOwner() const;

	void setOwner(Puppet & owner);

	virtual void attack();
	virtual bool canAttack() const;
	virtual ~Weapon() = default;

protected:

	b2World & world;
	Puppet * owner = nullptr;

private:

	void clearOwner();

	friend class WeaponOwner;
};

extern Weapon NullWeapon;
