#include "stdafx.h"

#include "PuppetJumper.hpp"

#include "Puppet.hpp"

PuppetJumper::PuppetJumper(Puppet & puppet) :
	puppet(puppet)
{

}

void PuppetJumper::setForceOfJump(float forceOfJump)
{
	this->forceOfJump = forceOfJump;
}

void PuppetJumper::jump()
{
	if (!canJump())
	{
		return;
	}

	puppet.setLinearVelocity(
		b2Vec2(puppet.getLinearVelocity().x, 0.0f)
	);

	puppet.applyLinearImpulseToCenter(
		b2Vec2(0.0f, forceOfJump)
	);

	noJumpPeriod.activateOn(sf::milliseconds(50));
}

bool PuppetJumper::canJump()
{
	return footSensorContactable.canJump() && noJumpPeriod.isFinished();
}
