#pragma once

#include <Box2D\Common\b2Math.h>
#include <SFML\System\Vector2.hpp>

class CoordinateSystem
{
public:
	CoordinateSystem() = delete;

	// Model -> View
	static sf::Vector2f transformToViewPosition(b2Vec2 position);
	static sf::Vector2f transformToViewDimensions(b2Vec2 dimensions);
	static float transformToViewAngle(float32 angle);

	// View -> Model
	static b2Vec2 transformToModelPosition(sf::Vector2f position);
	static b2Vec2 transformToModelDimensions(sf::Vector2f dimensions);
	static float32 transformToModelAngle(float angle);
};
