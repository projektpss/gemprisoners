#include "stdafx.h"

#include "SwordHitBox.hpp"

#include "Box2D/Box2D.h"

SwordHitBox::SwordHitBox(b2World & world, b2Vec2 position, DirectionOfLooking directionOfAttack):
	HitBox(world, position)
{
	float dir = directionOfAttack == DirectionOfLooking::Left ? -1.0f : 1.0f;

	b2Vec2 vertices[5];
	vertices[0].Set(dir * 0.0f,  0.0f);
	vertices[1].Set(dir * 0.4f, -0.5f);
	vertices[2].Set(dir * 0.6f, -0.3f);
	vertices[3].Set(dir * 0.6f,  0.3f);
	vertices[4].Set(dir * 0.4f,  0.5f);

	b2PolygonShape shape;
	shape.Set(vertices, 5);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.isSensor = true;
	fixtureDef.userData = this;
	fixture = body->CreateFixture(&fixtureDef);
}

Attack SwordHitBox::createAttack()
{
	return Attack(8, Attack::Type::Steel);
}
