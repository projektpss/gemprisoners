#pragma once

#include "Box2D/Common/b2Math.h"

#include "Contactable.hpp"
#include "Attack.hpp"

class b2World;
class b2Body;

class HitBox : public Contactable
{
public:

	HitBox(b2World & world, b2Vec2 position);
	virtual ~HitBox();

	b2Body & getBody();

	virtual Attack createAttack() = 0;

	void weldTo(b2Body & body);

protected:

	b2Body * body;

	virtual void handleBeginContact(Contactable & contactable) override;
	b2World & getWorld();
};
