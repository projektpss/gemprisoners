#pragma once

#include "Health.hpp"

class HealthChangeObserver
{
public:

	virtual void onChange(const Health & health);
};
