#pragma once

#include "SFML/Graphics/Color.hpp"
#include "SFML/Graphics/Drawable.hpp"
#include "SFML/Graphics/Font.hpp"
#include "SFML/System/Vector2.hpp"

class Health;

class HealthView : public sf::Drawable
{
public:

	HealthView(const Health & health, sf::Vector2f position);

	// Inherited via Drawable
	void draw(sf::RenderTarget & target, sf::RenderStates states) const override;

private:

	const Health & health;
	sf::Vector2f position;

	// To avoid loading font on every drawing
	sf::Font font;
};
