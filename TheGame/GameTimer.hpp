#pragma once

#include "SFML/System/Time.hpp"

class GameTimer
{
public:

	GameTimer() = delete;

	static sf::Time getTime();

	// TODO: Hide it from other users (e.g. move to private
	//       section and make avalible for one friend class).
	static void registerTimeLapse(sf::Time elapsedTime);

private:

	static sf::Time now;
};
