#pragma once

#include "HitBox.hpp"
#include "DirectionOfLooking.hpp"

class b2Fixture;

class SwordHitBox : public HitBox
{
public:

	SwordHitBox(b2World & world, b2Vec2 position, DirectionOfLooking directionOfAttack);

	Attack createAttack() override;

private:

	b2Fixture * fixture;
};
