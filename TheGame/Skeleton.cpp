#include "stdafx.h"

#include "Skeleton.hpp"

#include "Box2D/Box2D.h"

#include "Attack.hpp"

Skeleton::Skeleton(b2World & world, b2Vec2 position) :
	puppet(world, position, *this),
	health(20)
{

}

SkeletonPuppet & Skeleton::getPuppet()
{
	return puppet;
}

bool Skeleton::isAttackable() const
{
	return true;
}

void Skeleton::takeAttack(Attack & attack)
{
	health -= attack.getValue();
}
