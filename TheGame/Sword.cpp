#include "stdafx.h"

#include "Sword.hpp"

#include "Box2D/Box2D.h"

#include "GridPosition.hpp"
#include "Puppet.hpp"
#include "SwordHitBox.hpp"
#include "SwordObserver.hpp"
#include "TaskExecutor.hpp"

Sword::Sword(b2World & world):
	Weapon(world)
{

}

void Sword::attack()
{
	if (!canAttack())
	{
		return;
	}

	SwordHitBox * hitBox = new SwordHitBox(world, GridPosition(50, 50), owner->getDirectionOfLooking());
	notifyOnCreateObservers(*hitBox);

	TaskExecutor::executeAfter(
		sf::seconds(0.3f),
		[hitBox, this]
		{
			this->notifyOnDestroyObservers(*hitBox);
			delete hitBox;
		}
	);

	hitBox->weldTo(*owner->getBody());

	noAttackPeriod.activateOn(sf::seconds(0.5f));
}

bool Sword::canAttack() const
{
	return noAttackPeriod.isFinished();
}

void Sword::subscribe(SwordObserver & observer)
{
	observers.push_back(&observer);
}

void Sword::unsubscribe(SwordObserver & observer)
{
	observers.erase(
		std::remove(observers.begin(), observers.end(), &observer),
		observers.end()
	);
}

void Sword::notifyOnCreateObservers(SwordHitBox & hitBox)
{
	for (auto & observer : observers)
	{
		observer->onHitBoxCreate(hitBox);
	}
}

void Sword::notifyOnDestroyObservers(SwordHitBox & hitBox)
{
	for (auto & observer : observers)
	{
		observer->onHitBoxDestroy(hitBox);
	}
}
