#include "stdafx.h"

#include "Attack.hpp"

Attack::Attack(int value, Type type):
	value(value),
	type(type)
{

}

int Attack::getValue() const
{
	return value;
}

Attack::Type Attack::getType() const
{
	return type;
}
