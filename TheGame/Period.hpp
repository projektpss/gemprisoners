#pragma once

#include "SFML/System/Time.hpp"

class Period
{
public:

	Period(sf::Time activeTime = sf::Time());

	bool isActive() const;
	bool isFinished() const;
	void activateOn(sf::Time time);

private:

	sf::Time timeOut;
};
