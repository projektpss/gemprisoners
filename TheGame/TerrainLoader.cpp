#include "stdafx.h"

#include "TerrainLoader.hpp"

#include <fstream>
#include <exception>
#include <memory>

#include "GridPosition.hpp"
#include "GroundBlock.hpp"

void TerrainLoader::load(Terrain & terrain, std::string filename)
{
	std::ifstream file(filename, std::ios::in);

	if (!file.good())
	{
		throw std::invalid_argument("Can't open file: " + filename);
	}

	file >> std::noskipws;

	GridPosition position(0, 0);
	char c;
	while (file >> c)
	{
		if (c == '\n')
		{
			position.x = 0;
			position.y -= 1;

			continue;
		}

		switch (c)
		{
		case 'g':
			terrain.add(std::make_unique<GroundBlock>(terrain.getWorld(), position));
			break;

		case ' ':
			break;

		default:
			break;
		}

		position.x += 1;
	}
}
