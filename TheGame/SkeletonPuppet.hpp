#pragma once

#include "Controlable.hpp"
#include "Puppet.hpp"
#include "PuppetWalker.hpp"
#include "PuppetJumper.hpp"
#include "WeaponOwner.hpp"

class SkeletonPuppet :
	public Controlable,
	public Puppet,
	public PuppetWalker,
	public PuppetJumper,
	public WeaponOwner
{
public:

	SkeletonPuppet(b2World & world, b2Vec2 position, Contactable & mainContactable);

	// Inherited via Controlable
	int getType() const override;
	Walker * getWalker() override;
	Jumper * getJumper() override;
	Attacker * getAttacker() override;
};
