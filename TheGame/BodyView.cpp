#include "stdafx.h"

#include "BodyView.hpp"

#include "Drawer.hpp"

BodyView::BodyView(const b2Body & body, sf::Color color) :
	body(&body),
	color(color)
{

}

void BodyView::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	Drawer::drawBody(target, body, color);
}
