#pragma once

#include "Attacker.hpp"

class Puppet;
class Weapon;
extern Weapon NullWeapon;

class WeaponOwner : public Attacker
{
public:

	WeaponOwner(Puppet & puppet);
	virtual ~WeaponOwner();

	Weapon & getWeapon();
	void setWeapon(Weapon & weapon);

	// Inherited via Attacker
	void attack() override;
	bool canAttack() const override;

protected:

	Weapon * weapon = &NullWeapon;
	Puppet & puppet;
};
