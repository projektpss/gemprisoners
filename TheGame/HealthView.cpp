#include "stdafx.h"

#include "HealthView.hpp"

#include <sstream>

#include "SFML/Graphics.hpp"

#include "Health.hpp"

namespace
{

const float Width = 200.0f;
const float Height = 30.0f;
const int CharacterSize = 20;

const sf::Color    textColor(0xFF, 0xFF, 0xFF);
const sf::Color    lifeColor(0x90, 0x00, 0x00);
const sf::Color    backColor(0x30, 0x00, 0x00);
const sf::Color outlineColor(0x66, 0x66, 0x66);

}

HealthView::HealthView(const Health & health, sf::Vector2f position):
	health(health),
	position(position)
{
	font.loadFromFile("Fonts/Consolas/consola.ttf");
}

void HealthView::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	// Draw background
	sf::Vector2f rectSize(Width, Height);
	sf::RectangleShape rect(rectSize);
	rect.setPosition(position);
	rect.setFillColor(backColor);
	rect.setOutlineColor(outlineColor);
	rect.setOutlineThickness(1.0f);
	target.draw(rect, states);

	// Draw remaining life
	rectSize.x = Width * static_cast<float>(health.getAsPercentOfMaximum());
	rect.setSize(rectSize);
	rect.setFillColor(lifeColor);
	rect.setOutlineThickness(0.0f);
	target.draw(rect, states);

	// Write life as string
	std::stringstream str;
	str << health.getValue() << "/" << health.getMaximum();

	// Draw string
	sf::Text text(str.str(), font, CharacterSize);
	sf::FloatRect localBounds = text.getLocalBounds();
	text.setOrigin(localBounds.left, localBounds.top);
	text.setPosition(
		sf::Vector2f(
			position.x + ( Width - localBounds.width ) / 2,
			position.y + (Height - localBounds.height) / 2
		)
	);
	text.setFillColor(textColor);

	target.draw(text, states);
}
