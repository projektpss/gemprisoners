#include "stdafx.h"

#include "Controlable.hpp"

#include <stdexcept>

bool Controlable::isWalker() const
{
	return getType() & WalkerType;
}

bool Controlable::isJumper() const
{
	return getType() & JumperType;
}

bool Controlable::isAttacker() const
{
	return getType() & AttackerType;
};

Walker * Controlable::getWalker()
{
	throw std::runtime_error("Can't get Walker from that class");
}

Jumper * Controlable::getJumper()
{
	throw std::runtime_error("Can't get Jumper from that class");
}

Attacker * Controlable::getAttacker()
{
	throw std::runtime_error("Can't get Attacker from that class");
}
