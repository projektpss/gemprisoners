#include "stdafx.h"

#include "Contactable.hpp"

void Contactable::handleBeginContact(Contactable & contactable)
{
	// Do nothing
}

void Contactable::handleEndContact(Contactable & contactable)
{
	// Do nothing
}

namespace
{

Contactable instance;

}

Contactable * const NullContactable = &instance;
