#pragma once

#include "Jumper.hpp"
#include "FootSensorContactable.hpp"
#include "Period.hpp"

class Puppet;

class PuppetJumper : public Jumper
{
public:

	PuppetJumper(Puppet & puppet);

	void setForceOfJump(float forceOfJump);

	// Inherited via Jumper
	void jump() override;
	bool canJump() override;

protected:

	FootSensorContactable footSensorContactable;

private:

	Puppet & puppet;

	float forceOfJump = 0.0f;
	Period noJumpPeriod;
};
