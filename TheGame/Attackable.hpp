#pragma once

class Attack;

class Attackable
{
public:

	virtual bool isAttackable() const;
	virtual void takeAttack(Attack & attack);

	virtual ~Attackable() = default;
};
