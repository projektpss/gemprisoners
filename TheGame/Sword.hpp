#pragma once

#include <vector>

#include "Weapon.hpp"
#include "Period.hpp"

class SwordHitBox;
class SwordObserver;

class Sword : public Weapon
{
public:

	Sword(b2World & world);

	void attack() override;
	bool canAttack() const override;

	void subscribe(SwordObserver & observer);
	void unsubscribe(SwordObserver & observer);

private:

	Period noAttackPeriod;

	std::vector<SwordObserver *> observers;

	void notifyOnCreateObservers(SwordHitBox & hitBox);
	void notifyOnDestroyObservers(SwordHitBox & hitBox);
};
