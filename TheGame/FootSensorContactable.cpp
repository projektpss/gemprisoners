#include "stdafx.h"

#include "FootSensorContactable.hpp"

void FootSensorContactable::handleBeginContact(Contactable & contactable)
{
	jumpablesCnt += (contactable.isJumpable()) ? 1 : 0;
}

void FootSensorContactable::handleEndContact(Contactable & contactable)
{
	jumpablesCnt -= (contactable.isJumpable()) ? 1 : 0;
	jumpablesCnt = (jumpablesCnt < 0) ? 0 : jumpablesCnt;
}

bool FootSensorContactable::canJump()
{
	return (jumpablesCnt > 0) ? true : false;
}
