#pragma once

#include <SFML\Graphics\RenderWindow.hpp>
#include <Box2D\Dynamics\b2Body.h>

class Drawer
{
public:
	Drawer() = delete;

	static void drawBody(sf::RenderTarget & target, const b2Body * body, sf::Color color);
};
