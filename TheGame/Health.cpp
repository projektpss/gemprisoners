#include "stdafx.h"

#include <algorithm>

#include "Health.hpp"
#include "HealthChangeObserver.hpp"
#include "DeadObserver.hpp"

Health::Health(int maximum) :
	value(maximum),
	maximum(maximum)
{

}

Health::Health(int value, int maximum) :
	maximum(maximum)
{
	this->value = limitGivenValueToRange(value);
}

int Health::getValue() const
{
	return value;
}

int Health::getMaximum() const
{
	return maximum;
}

void Health::setValue(int value)
{
	value = limitGivenValueToRange(value);

	if (value == this->value)
	{
		return;
	}

	this->value = value;
	notifyOnChangeObservers();

	if (this->value == 0)
	{
		notifyOnDeadObservers();
	}
}

void Health::setMaximum(int maximum)
{
	if (this->maximum == maximum)
	{
		return;
	}

	if (value > maximum)
	{
		value = maximum;
	}

	this->maximum = maximum;
	notifyOnChangeObservers();
}

double Health::getAsPercentOfMaximum() const
{
	return static_cast<double>(value) / maximum;
}

void Health::subscribe(HealthChangeObserver & observer)
{
	healthChangeObservers.push_back(&observer);
}

void Health::unsubscribe(HealthChangeObserver & observer)
{
	healthChangeObservers.erase(
		std::remove(healthChangeObservers.begin(), healthChangeObservers.end(), &observer),
		healthChangeObservers.end()
	);
}

Health & Health::operator+=(int healing)
{
	setValue(value + healing);
	return *this;
}

Health & Health::operator-=(int damage)
{
	setValue(value - damage);
	return *this;
}

int Health::limitGivenValueToRange(int value) const
{
	if (value > maximum)
	{
		return maximum;
	}
	else if (value < 0)
	{
		return 0;
	}

	return value;
}

void Health::notifyOnChangeObservers() const
{
	for (auto & observer : healthChangeObservers)
	{
		observer->onChange(*this);
	}
}

void Health::notifyOnDeadObservers() const
{
	for (auto & observer : deadObservers)
	{
		observer->onDead();
	}
}
