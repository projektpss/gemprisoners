#pragma once

#include "Box2D/Common/b2Math.h"

#include "DirectionOfLooking.hpp"

class b2Body;
class b2World;
struct b2FixtureDef;

class Puppet
{
public:

	Puppet(b2World & world, b2Vec2 position);
	virtual ~Puppet();

	// TODO: Remove this getter later
	b2Body * getBody();

	b2Vec2 getPosition() const;
	b2Vec2 getLinearVelocity() const;
	DirectionOfLooking getDirectionOfLooking() const;

	void setDirectionOfLooking(DirectionOfLooking directionOfLooking);

	void applyForceToCenter(b2Vec2 force);
	void applyLinearImpulseToCenter(b2Vec2 impulse);
	void setLinearVelocity(b2Vec2 velocity);

	void createFixture(b2FixtureDef & fixtureDef);

protected:

	b2Body * body;
	DirectionOfLooking directionOfLooking = DirectionOfLooking::Left;
};
