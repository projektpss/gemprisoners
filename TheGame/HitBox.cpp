#include "stdafx.h"

#include "HitBox.hpp"

#include <limits>

#include "Box2D/Box2D.h"

HitBox::HitBox(b2World & world, b2Vec2 position)
{
	b2BodyDef bodyDef;
	bodyDef.position = position;
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);

	b2MassData massData;
	body->GetMassData(&massData);
	massData.mass = std::numeric_limits<float>::epsilon();
	massData.I = std::numeric_limits<float>::epsilon();
	body->SetMassData(&massData);
}

HitBox::~HitBox()
{
	body->GetWorld()->DestroyBody(body);
}

b2Body & HitBox::getBody()
{
	return *body;
}

void HitBox::weldTo(b2Body & body)
{
	b2WeldJointDef jointDef;
	jointDef.bodyA = this->body;
	jointDef.bodyB = &body;
	getWorld().CreateJoint(&jointDef);
}

void HitBox::handleBeginContact(Contactable & contactable)
{
	if (contactable.isAttackable())
	{
		Attack attack = createAttack();

		Attackable & attackable = contactable;
		attackable.takeAttack(attack);
	}
}

b2World & HitBox::getWorld()
{
	return *body->GetWorld();
}
