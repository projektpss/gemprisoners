#pragma once

#include "Terrain.hpp"
#include "TerrainView.hpp"

class TerrainController
{
public:

	TerrainController(Terrain & model, TerrainView & view);
	void loadFromFile(std::string filename);

private:

	Terrain & model;
	TerrainView & view;
};
